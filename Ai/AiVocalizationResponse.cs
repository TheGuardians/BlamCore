using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0xC)]
    public class AiVocalizationResponse
    {
        [TagField(Label = true)]
        public StringId VocalizationName;
        public AiVocalizationResponseFlags Flags;
        public short VocalizationIndex;
        public AiVocalizationResponseType ResponseType;
        public short ImportDialogueIndex;
    }
}

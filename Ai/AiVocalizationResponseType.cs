namespace BlamCore.Ai
{
    public enum AiVocalizationResponseType : short
    {
        Friend,
        Enemy,
        Listener,
        Joint,
        Peer,
        Leader,
        FriendInfantry
    }
}

namespace BlamCore.Bitmaps
{
    public enum BitmapCurveMode : sbyte
    {
        ChooseBest,
        ForceFast,
        ForcePretty
    }
}

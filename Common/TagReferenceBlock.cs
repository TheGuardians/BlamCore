﻿using BlamCore.Cache;
using BlamCore.Serialization;

namespace BlamCore.Common
{
    [TagStructure(Size = 0x10)]
    public struct TagReferenceBlock
    {
        [TagField(Label = true)]
        public CachedTagInstance Instance;
    }
}
﻿namespace BlamCore.Shaders
{
    public enum ShaderParameterRegisterType : sbyte
    {
        Boolean,
        Integer,
        Float,
        Sampler
    }
}
using BlamCore.Cache;
using BlamCore.Serialization;
using BlamCore.Shaders;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "vertex_shader", Tag = "vtsh", Size = 0x20, MinVersion = CacheVersion.Halo3Retail)]
	public class VertexShader
	{
		public uint Unknown;
		public List<DrawModeList> DrawModeLists;
		public uint Unknown3;
		public List<ShaderData> Shaders;

		[TagStructure(Size = 0xC)]
		public class DrawModeList
		{
			public List<ShaderDrawMode> DrawModes;
		}
	}
}